package com.zhaowa.course.design.strategy;

import org.junit.Test;

import java.util.concurrent.atomic.AtomicLong;

public class Main {


    @Test
    public void testFactory() {
        CashSuper cs = CashFactory.createCashAccept("打8折");
        double result = cs.acceptCash(300);
        System.out.println(result);
    }

    @Test
    public void testContext() {
        CashContext context = null;
        double money = 0.0;
        String strategy = "打8折";
        switch (strategy) {
            case "正常收费":
                context = new CashContext(new CashNormal());
                break;
            case "打8折":
                context = new CashContext(new CashRebate(8));
                break;
            case "满300减100":
                context = new CashContext(new CashReturn(300, 100));
                break;

            default:
                break;
        }

        money = context.getResult(300);
        System.out.println(money);
    }

    @Test
    public void testCashContextStrategy() {
        CashContextStrategy context = null;
        double money = 0.0;
        String strategy = "打8折";
        context = new CashContextStrategy(strategy);
        money = context.getResult(300);
        System.out.println(money);
    }

    @Test
    public void test() {
        CashContextReflect context = null;
        double money = 0.0;
        String type = "com.zhaowa.course.design.strategy.CashRebate";
        Class[] paramTypes = {double.class};
        Object[] params = {8.0};
        context = new CashContextReflect(type, paramTypes, params);
        money = context.getResult(300);
        System.out.println(money);
    }
}